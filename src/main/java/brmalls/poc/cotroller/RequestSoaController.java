package brmalls.poc.cotroller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.brmalls.soa.transaction.getlistaclientesinadimplentes.v2.CustAccountRec;
import brmalls.poc.model.ClientesInadimplentes;
import brmalls.poc.proxy.ClientesInadimplentesSoaProxy;

@RestController
@RequestMapping("/soa")
public class RequestSoaController {

	@Autowired
	ClientesInadimplentesSoaProxy clientesInadimplentesProxy;
	
	@GetMapping("/{shopping}/{mesRef}/{dueDate}")
	public List<ClientesInadimplentes> primeiroFiltro(@PathVariable("shopping") String shopping,@PathVariable("mesRef") String mesRef,@PathVariable("dueDate") String dueDate) throws Exception{
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

		Date dueDateFormat;
		try {
			dueDateFormat = formatter.parse(dueDate);
		} catch (ParseException e) {
			throw new Exception("Erro ao converter data");			
		}

		
		List<CustAccountRec> clientes = clientesInadimplentesProxy.resquestGet(shopping, mesRef, dueDateFormat);
		List<ClientesInadimplentes> result = new ArrayList<>();
		clientes.forEach((c)->{
			result.add(converter(c));
		});
		
		return result;		
	}
	
	private ClientesInadimplentes converter(CustAccountRec c) {
		ClientesInadimplentes cliente = new ClientesInadimplentes(c);
		return cliente;
	}

	@GetMapping("/{mesRef}/{dueDate}")
	public List<ClientesInadimplentes> segudoFiltro(@PathVariable("mesRef") String mesRef,@PathVariable("dueDate") String dueDate) throws Exception{
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

		List<BigDecimal> customerIds = new ArrayList<BigDecimal>();
		Date dueDateFormat;
		try {
			dueDateFormat = formatter.parse(dueDate);
		} catch (ParseException e) {
			throw new Exception("Erro ao converter data");			
		}
		
		customerIds.add(new BigDecimal(7854049));
		customerIds.add(new BigDecimal(2594735));
		customerIds.add(new BigDecimal(4547739));
		customerIds.add(new BigDecimal(8259137));
		customerIds.add(new BigDecimal(6400125));
		customerIds.add(new BigDecimal(10008157));
		customerIds.add(new BigDecimal(7451));
		customerIds.add(new BigDecimal(11818160));
		customerIds.add(new BigDecimal(7854038));
		customerIds.add(new BigDecimal(7848138));
		customerIds.add(new BigDecimal(521917));
		customerIds.add(new BigDecimal(295695));
		
		List<CustAccountRec> clientes = clientesInadimplentesProxy.resquestPost(mesRef, dueDateFormat, customerIds);
		List<ClientesInadimplentes> result = new ArrayList<>();
		clientes.forEach((c)->{
			result.add(converter(c));
		});
		
		return result;		
	}
	
	@GetMapping
	public String ok(){
		return "OK";
	}
	

}
