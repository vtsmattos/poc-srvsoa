package brmalls.poc;

import javax.xml.ws.BindingProvider;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.oracle.sca.soapservice.apiservicesapp.soabrmallscustomer.soabrmallscustomer.SOAbrMallsCustomerPtt;
import com.oracle.sca.soapservice.apiservicesapp.soabrmallscustomer.soabrmallscustomer.SOAbrMallsCustomerService;

@Configuration
@EnableWebMvc
@EnableAutoConfiguration
@EnableCircuitBreaker
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class SrvSoaApplication {
	public static void main(String[] args) {
		SpringApplication.run(SrvSoaApplication.class, args);
	}
	
	@Value("${brMalls.soa.path.endpoint}")
	private String endPoint;

	//@LoadBalanced
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public SOAbrMallsCustomerPtt getSOAbrMallsPort() {

		SOAbrMallsCustomerService service = new SOAbrMallsCustomerService();
		SOAbrMallsCustomerPtt port = service.getSOAbrMallsCustomerPt();
		BindingProvider bp = (BindingProvider) port;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
		return port;
	}
}
