package brmalls.poc.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import br.com.brmalls.soa.transaction.getlistaclientesinadimplentes.v2.CustAccountRec;

public class ClientesInadimplentes {

	private Long customerId;
	private String shopping;
	private String mesRef;
	private Date dueDate;

	public ClientesInadimplentes() {}

	public ClientesInadimplentes(CustAccountRec c) {
		if(Objects.nonNull(c.getCustAccountId())){
			this.customerId = c.getCustAccountId().setScale(0,BigDecimal.ROUND_UP).longValueExact();
		}
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getShopping() {
		return shopping;
	}

	public void setShopping(String shopping) {
		this.shopping = shopping;
	}

	public String getMesRef() {
		return mesRef;
	}

	public void setMesRef(String mesRef) {
		this.mesRef = mesRef;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	
}
