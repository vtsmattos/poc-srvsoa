package brmalls.poc.proxy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.oracle.sca.soapservice.apiservicesapp.soabrmallscustomer.soabrmallscustomer.SOAbrMallsCustomerPtt;

import br.com.brmalls.soa.transaction.getlistaclientesinadimplentes.v2.CustAccountIdList;
import br.com.brmalls.soa.transaction.getlistaclientesinadimplentes.v2.CustAccountRec;
import br.com.brmalls.soa.transaction.getlistaclientesinadimplentes.v2.CustAccountTab;
import br.com.brmalls.soa.transaction.getlistaclientesinadimplentes.v2.GetListaClientesInadimplentes;
import br.com.brmalls.soa.transaction.getlistaclientesinadimplentes.v2.GetListaClientesInadimplentesOutputParameters;
import br.com.brmalls.soa.transaction.getlistaclientesinadimplentes.v2.ObjectFactory;

@Service
public class ClientesInadimplentesSoaProxy {

	@Autowired
	private Environment env;
	
	//http://localhost:54376/api/ClienteInadiplente2/get/?shopping=NORTESHOPPING&dataInicio=01-04-2019&dataFim=30-04-2019

	@Autowired
	private SOAbrMallsCustomerPtt soaApi;
	
	@HystrixCommand(fallbackMethod = "resquestGetFallback", commandProperties = {
			@HystrixProperty(name = "execution.isolation.strategy", value = "THREAD"),
			@HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),
			@HystrixProperty(name = "requestCache.enabled", value = "false"), }, threadPoolProperties = {
					@HystrixProperty(name = "coreSize", value = "5"),
					@HystrixProperty(name = "maximumSize", value = "5") })
	public List<CustAccountRec> resquestGet(String shopping, String mesRef, Date dueDate) {
		
		try {
			ObjectFactory factory = new ObjectFactory();
			String url = env.getProperty("environments.url");
			if(Objects.isNull(url)){
				//se nao conseguir ler do config server
				url = "http://localhost:54376/api/ClienteInadiplente2/get/";
			}
			
			GetListaClientesInadimplentes payload = factory.createGetListaClientesInadimplentes();
			payload.setMesAno(mesRef);			
			payload.setShopping(shopping);
			payload.setDtCorte(convertToXMLDate(dueDate));
			
			
			GetListaClientesInadimplentesOutputParameters output = soaApi.getListaClientesInadimplentes(payload);
			
			CustAccountTab custAccountTab = output.getCustAccountTab();
			
			return custAccountTab.getCustAccountItem();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	List<CustAccountRec> resquestGetFallback(String shopping, String mesRef, Date dueDate) {
		return new ArrayList<CustAccountRec>();
	}

	@HystrixCommand(fallbackMethod = "resquestPostFallback", commandProperties = {
			@HystrixProperty(name = "execution.isolation.strategy", value = "THREAD"),
			@HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "5"),
			@HystrixProperty(name = "requestCache.enabled", value = "false"), }, threadPoolProperties = {
					@HystrixProperty(name = "coreSize", value = "5"),
					@HystrixProperty(name = "maximumSize", value = "5") })
	public List<CustAccountRec> resquestPost(String mesRef, Date dueDate, List<BigDecimal> customerIds) {
		
		try {
			ObjectFactory factory = new ObjectFactory();
			String url = env.getProperty("environments.url");
			if(Objects.isNull(url)){
				//se nao conseguir ler do config server
				url = "http://localhost:54376/api/ClienteInadiplente2/get/";
			}
			
			CustAccountIdList createCustAccountIdList = factory.createCustAccountIdList();
			
			for (BigDecimal id : customerIds) {
				createCustAccountIdList.getCustAccountId().add(id);
			}
			
			GetListaClientesInadimplentes payload = factory.createGetListaClientesInadimplentes();
			payload.setMesAno(mesRef);			
			payload.setCustAccountIdList(createCustAccountIdList);
			payload.setDtCorte(convertToXMLDate(dueDate));
			
			
			GetListaClientesInadimplentesOutputParameters output = soaApi.getListaClientesInadimplentes(payload);
			
			CustAccountTab custAccountTab = output.getCustAccountTab();
			
			return custAccountTab.getCustAccountItem();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	List<CustAccountRec> resquestPostFallback(String mesRef, Date dueDate, List<BigDecimal> customerIds) {
		return new ArrayList<CustAccountRec>();
	}
	
	

	private XMLGregorianCalendar convertToXMLDate(Date date) {
			   if(date == null){
				   return null;
			   }
			   
		       GregorianCalendar gCalendar = new GregorianCalendar();
		       gCalendar.setTime(date);
		       XMLGregorianCalendar xmlCalendar = null;
		       try {
		           xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
		       } catch (DatatypeConfigurationException ex) {
		           ex.printStackTrace();
		       }
		       return xmlCalendar;
		   }

}
